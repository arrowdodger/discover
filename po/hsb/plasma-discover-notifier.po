# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the discover package.
#
# Edward Wornar <edi.werner@gmx.de>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-11-14 00:47+0000\n"
"PO-Revision-Date: 2021-10-29 12:53+0200\n"
"Last-Translator: Edward Wornar <edi.werner@gmx.de>\n"
"Language-Team: Upper Sorbian <kde-i18n-doc@kde.org>\n"
"Language: hsb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n"
"%100==4 ? 2 : 3;\n"

#: notifier/DiscoverNotifier.cpp:144
#, kde-format
msgid "View Updates"
msgstr "Aktualizowanja pokazać"

#: notifier/DiscoverNotifier.cpp:261
#, kde-format
msgid "Security updates available"
msgstr "Wěstotne aktualizowanja steja k dispoziciji"

#: notifier/DiscoverNotifier.cpp:263
#, kde-format
msgid "Updates available"
msgstr "Aktualizowanja steja k dispoziciji"

#: notifier/DiscoverNotifier.cpp:265
#, kde-format
msgid "System up to date"
msgstr "Sytem je na aktualnym stawje"

#: notifier/DiscoverNotifier.cpp:267
#, kde-format
msgid "Computer needs to restart"
msgstr "Kompjuter dyrbi so znowa startować"

#: notifier/DiscoverNotifier.cpp:269
#, kde-format
msgid "Offline"
msgstr "Offline"

#: notifier/DiscoverNotifier.cpp:271
#, kde-format
msgid "Applying unattended updates…"
msgstr "Nałožuju awtomatiske aktualizowanja..."

#: notifier/DiscoverNotifier.cpp:305
#, kde-format
msgid "Restart is required"
msgstr "Kompjuter dyrbi so znowa startować"

#: notifier/DiscoverNotifier.cpp:306
#, kde-format
msgid "The system needs to be restarted for the updates to take effect."
msgstr ""
"System dyrbi so znowa startować, zo bychu so aktualizowanja wobkedźbowali."

#: notifier/DiscoverNotifier.cpp:312
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "Znowa startować"

#: notifier/DiscoverNotifier.cpp:332
#, kde-format
msgctxt "@action:button"
msgid "Upgrade"
msgstr "Zlěpšić"

#: notifier/DiscoverNotifier.cpp:333
#, kde-format
msgid "Upgrade available"
msgstr "Zlěpšenje steji k dispoziciji"

#: notifier/DiscoverNotifier.cpp:334
#, kde-format
msgid "New version: %1"
msgstr "Nowa wersija: %1"

#: notifier/main.cpp:39
#, kde-format
msgid "Discover Notifier"
msgstr "Discover skedźbnjenje"

#: notifier/main.cpp:41
#, kde-format
msgid "System update status notifier"
msgstr "Skedźbnjenje na staw aktualizowanja"

#: notifier/main.cpp:43
#, kde-format
msgid "© 2010-2022 Plasma Development Team"
msgstr ""

#: notifier/main.cpp:47
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Edward Wornar"

#: notifier/main.cpp:47
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "ewerner@uni-leipzig.de"

#: notifier/main.cpp:52
#, kde-format
msgid "Replace an existing instance"
msgstr "Naruna ekstistowacu instancu"

#: notifier/main.cpp:54
#, kde-format
msgid "Do not show the notifier"
msgstr "Njepokaž skedźbnjenja"

#: notifier/main.cpp:54
#, kde-format
msgid "hidden"
msgstr "chowane"

#: notifier/NotifierItem.cpp:35 notifier/NotifierItem.cpp:36
#, kde-format
msgid "Updates"
msgstr "Aktualizowanja"

#: notifier/NotifierItem.cpp:50
#, kde-format
msgid "Open Discover…"
msgstr "Discover wočinić..."

#: notifier/NotifierItem.cpp:55
#, kde-format
msgid "See Updates…"
msgstr "Pokaž aktualizowanja…"

#: notifier/NotifierItem.cpp:60
#, kde-format
msgid "Refresh…"
msgstr "Napohlad ponowić..."

#: notifier/NotifierItem.cpp:64
#, kde-format
msgid "Restart to apply installed updates"
msgstr "Znowa startować za nałoženje instalowanych aktualizowanjow"

#: notifier/NotifierItem.cpp:65
#, kde-format
msgid "Click to restart the device"
msgstr "Klikńće, zo byšće grat znowa startowali"
